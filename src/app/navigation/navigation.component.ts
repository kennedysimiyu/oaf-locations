import { Component, OnInit } from '@angular/core';

import { GroupsRepositoryService } from '../shared/groupsrepository.service';

import { Country, District, Season, Site, Group } from '../model/entity-model';

@Component({
  selector: 'oaf-location-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  private selectedCountry: Country;
  private selectedDistrict: District;
  private selectedSeason: Season;

  public selectedCountryId = 0;
  public selectedDistrictName = "";
  public selectedDistrictId = 0;

  constructor(private _groupsRepositoryService: GroupsRepositoryService) { }

    countries: Country[];
    districts: District[];
    seasons: Season[];
  ngOnInit() {
    this._groupsRepositoryService.getCountries().then(countries => this.countries = countries,
                                      error => console.error(error));
    this._groupsRepositoryService.getDistricts().then(districts => this.districts = districts,
                                        error => console.error(error));
    this._groupsRepositoryService.getSeasons().then(seasons => this.seasons = seasons,
                                          error => console.error(error));
   }

   save() {
     this._groupsRepositoryService.saveChanges().then(() => {
       this.ngOnInit();
     }, error => console.error(error));
   }

   onSelectCountry(country: Country) {
    this.selectedCountry = country;
   }

   onSelectDistrict(district: District) {
    this.selectedDistrict = district;
   }

   onSelectSeason(season: Season) {
    this.selectedSeason = season;
   }

   getDistricts() {

    let districts = [];
    if (this.selectedCountryId > 0 && this.countries.length > 0) {
        let countries = this.countries.filter(c => (c.CountryId == this.selectedCountryId));
        let regions = countries[0].Regions;
        for (var r in regions) {
            let rDistricts = regions[r].Districts;
            if (rDistricts != null) {
                districts = districts.concat(rDistricts);
            }
        }
    } else {
        //do nothing
    }

    this.districts = districts;
  }

}


