import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistrictNavigationSelectionComponent } from './district-navigation-selection.component';

describe('DistrictNavigationSelectionComponent', () => {
  let component: DistrictNavigationSelectionComponent;
  let fixture: ComponentFixture<DistrictNavigationSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistrictNavigationSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistrictNavigationSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

 /* it('should create', () => {
    expect(component).toBeTruthy();
  }); */
});
