import { Component, OnInit, Input } from '@angular/core';

import { District } from '../../model/district';

@Component({
  selector: 'oaf-district-navigation-selection',
  templateUrl: './district-navigation-selection.component.html',
  styleUrls: ['./district-navigation-selection.component.css']
})
export class DistrictNavigationSelectionComponent implements OnInit {

  private isSelected: boolean;

    constructor() { }

    @Input()
    public district: District;

    @Input()
    public set selectedDistrict(value: District) {
      if (value === this.district) {
        this.isSelected = true;
      }
      else {
        this.isSelected = false;
      }
    }

    ngOnInit() {
    }

  }
