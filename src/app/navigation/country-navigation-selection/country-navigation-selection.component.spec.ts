import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryNavigationSelectionComponent } from './country-navigation-selection.component';

describe('CountryNavigationSelectionComponent', () => {
  let component: CountryNavigationSelectionComponent;
  let fixture: ComponentFixture<CountryNavigationSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountryNavigationSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryNavigationSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

 /* it('should create', () => {
    expect(component).toBeTruthy();
  }); */
});
