import { Component, OnInit, Input } from '@angular/core';

import { Country } from '../../model/country';

@Component({
  selector: 'oaf-country-navigation-selection',
  templateUrl: './country-navigation-selection.component.html',
  styleUrls: ['./country-navigation-selection.component.css']
})

export class CountryNavigationSelectionComponent implements OnInit {
    private isSelected: boolean;

    constructor() { }

    @Input()
    public country: Country;

    @Input()
    public set selectedCountry(value: Country) {
      if (value === this.country) {
        this.isSelected = true;
      }
      else {
        this.isSelected = false;
      }
    }

    ngOnInit() {
    }

  }
