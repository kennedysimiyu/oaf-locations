import { CUSTOM_ELEMENTS_SCHEMA, DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';

import { async, fakeAsync, ComponentFixture, TestBed, tick } from '@angular/core/testing';

import { By } from '@angular/platform-browser';

import { SeasonNavigationSelectionComponent } from './season-navigation-selection.component';
import { GroupsRepositoryService } from '../shared/groupsrepository.service';

describe('SeasonNavigationSelectionComponent', () => {
  let component: SeasonNavigationSelectionComponent;
  let fixture: ComponentFixture<SeasonNavigationSelectionComponent>;

  let spy: jasmine.Spy;
  let de: DebugElement;
  let el: HTMLElement;
  let groupsRepositoryService: GroupsRepositoryService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeasonNavigationSelectionComponent ],
      schemas:      [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonNavigationSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  }); */
});
