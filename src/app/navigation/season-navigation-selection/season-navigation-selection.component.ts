import { Component, OnInit, Input } from '@angular/core';

import { Season } from '../../model/season';

@Component({
  selector: 'oaf-season-navigation-selection',
  templateUrl: './season-navigation-selection.component.html',
  styleUrls: ['./season-navigation-selection.component.css']
})
export class SeasonNavigationSelectionComponent implements OnInit {
  private isSelected: boolean;

  constructor() { }

  @Input()
  public season: Season;

  @Input()
  public set selectedSeason(value: Season) {
    if (value === this.season) {
      this.isSelected = true;
    }
    else {
      this.isSelected = false;
    }
  }

  ngOnInit() {
  }

}
