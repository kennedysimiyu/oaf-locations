
 // core imported modules for the routing module
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// imported components for where our app will naviagte to.
import { GroupsListComponent } from './groups/groups-list.component';
import { SectorsListComponent } from './sectors/sectors-list.component';
import { SitesListComponent } from './sites/sites-list.component';

import { InitGuard } from './shared/init.guard';

// routing module core navigation parmeters and route paths declartions
const routes: Routes = [
  {
    path: '', canActivateChild: [InitGuard], children: [

      // default application routing
      { path: '', component: GroupsListComponent },

      // groups page routing
      { path: 'group-list', component: GroupsListComponent },
      { path: 'group-list/:countryId', component: GroupsListComponent },

      // sites page routing
      { path: 'site-list', component: SitesListComponent },
      { path: 'site-list/:countryId', component: SitesListComponent},

      // sectors page routing
      { path: 'sector-list', component: SectorsListComponent },
      { path: 'sector-list/:countryId', component: SectorsListComponent}
    ]
  }
]


@NgModule({
  imports:  [RouterModule.forRoot(routes)],
  exports:  [RouterModule]
})
export class AppRoutingModule {}

