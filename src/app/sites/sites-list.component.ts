/* main sites component  */
// imported core angular modules
import { Component, OnInit, Input } from '@angular/core';

// imported services
import { GroupsRepositoryService } from '../shared/groupsrepository.service';

// imported models/entities
import { Country, District, Season, Site, Group } from '../model/entity-model';


@Component({
  selector: 'oaf-sites-list',
  templateUrl: './sites-list.component.html',
  styleUrls: ['./sites-list.component.css']
})

export class SitesListComponent implements OnInit {

  // class properties that show from the naviagtion the selected District, Country and season
  private selectedCountry: Country;
  public selectedCountryId = 0;

  private selectedDistrict: District;
  private selectedSeason: Season;

  // declaration of the groupsRepository Service instance that gets dependency injected, and declares a member variable in one statement
  constructor(private _groupsRepositoryService: GroupsRepositoryService) {}

 // data that is being entered in our component
  @Input()
  countries: Country[];

  // class property, array of sites
  @Input()
  sites: Site[];

  @Input()
  CountryId: number;

  // method called by angular when you component is loaded it can be placed on the view or through client side routing.
  ngOnInit() {

    // call a method on the groups repository service to get the sites. this method handles the promise
     this._groupsRepositoryService.getSites().then(sites => {
       this.sites = sites;
      }, error => console.error(error));

      // call a method on the groups repository service to get the countries. this method handles the promise
      this._groupsRepositoryService.getCountries().then(countries => {
        this.countries = countries;
      },
      error => console.error(error));
    }

    // a method that get the selected country from the country selector
    selectCountry(country: Country) {
      this.selectedCountry = country;
    }

   // saving of changes on our data repsotory
    save() {
      this._groupsRepositoryService.saveChanges().then(() => {
        this.ngOnInit();
      }, error => console.error(error));
    }
  }
