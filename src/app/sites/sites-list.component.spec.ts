import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';

import { async, ComponentFixture, TestBed, inject, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { GroupsRepositoryService } from '../shared/groupsrepository.service';


import { SitesListComponent } from './sites-list.component';
import { NavigationComponent } from '../navigation/navigation.component';

import { Country, District, Season, Site, Group } from '../model/entity-model';


describe('SitesListComponent', () => {
  let component: SitesListComponent;
  let fixture: ComponentFixture<SitesListComponent>;
  let componentGroupsRepositoryService: GroupsRepositoryService; // the actually injected service

  let spy: jasmine.Spy;
  let de: DebugElement;
  let el: HTMLElement;
  let groupsRepositoryService: GroupsRepositoryService; // the actually injected service

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
                      SitesListComponent,
                      NavigationComponent
                    ],
      schemas:      [ NO_ERRORS_SCHEMA ],
      providers:    [ {provide: GroupsRepositoryService, useValue: groupsRepositoryService } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SitesListComponent);
    component = fixture.componentInstance;
    // UserService actually injected into the component
    groupsRepositoryService = fixture.debugElement.injector.get(GroupsRepositoryService);
    componentGroupsRepositoryService = groupsRepositoryService;
    // UserService from the root injector
    groupsRepositoryService = TestBed.get(GroupsRepositoryService);
    fixture.detectChanges();
  });

 /* it('should create', () => {
    expect(component).toBeTruthy();
  }); */
});
