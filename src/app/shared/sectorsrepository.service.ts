// imports for core angular components for the service
import { Injectable } from '@angular/core';

// imports for data model definations
import { Sector } from '../model/sector';

@Injectable()
export class SectorsRepositoryService {

  constructor() {}

  // get sector method that returns a promise for a sector array
  getSectors(): Promise<Sector[]> {
  // constructing a promise and returning it.
  let promise = new Promise<Sector[]>((resolve, reject) => {

  });
  return promise;

  }
}
