import { Pipe, PipeTransform } from '@angular/core';

import { Group, Site } from '../model/entity-model';
@Pipe({
  name: 'groupconcatsites'
})
export class GroupconcatsitesPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let groups = value as Group[];
    let result = "";
    if (groups){
      let first: boolean = true;
      groups.forEach(gps => {
        if (!first) result += ",";
        first = false;
        result += gps.Site.Name;
      });
    }
    return result;
  }

}
