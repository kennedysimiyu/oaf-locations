// imports for core angular components for the service
import { Injectable } from '@angular/core';

// imported breeze apis and methods
import {
          EntityManager, EntityQuery, Predicate, Validator,
          FilterQueryOp, FetchStrategy, FetchStrategySymbol,
          EntityState, Entity, core
        } from 'breeze-client';

/// use entity-model class "acting as a barrel module instead of importing several lines" ///
import { Country, District, Season, Site, Group } from '../model/entity-model';

// imports for data model definations
import { RegistrationHelper } from '../model/registration-helper';
@Injectable()
export class GroupsRepositoryService {
  // create an instance of an entity manager as a member variable to the class,
  // it takes the url to our API endpoint.
  private _em: EntityManager = new EntityManager(
    'http://localhost:5660/breeze/Group'
  );
  // create a functionality to handle the cache of your data with a boolean property
  private _groupsCached: boolean;

  // create a functionality for initializing our service and give it a boolean property
  private _initialized: boolean;

  // register our application metadata
  constructor() {
    RegistrationHelper.register(this._em.metadataStore);
  }

  // create a functionality for initializing our service and give it a boolean property
  // fetch our metadata before initializng our app to avoid errors on breeze entity properties
  initialize() {
    let promise = new Promise<boolean>((resolve, reject) => {
      if (this._initialized) resolve(true);
      else {
        this._initialized = true;
        this._em.fetchMetadata().then(
          _ => {
            resolve(true);
          },
          error => console.error(error)
        );
      }
    });
    return promise;
  }

  // our create entity method for creating a new group entity ang giving it keys
  createEntity(entityType: string): Entity {
    let options: any = {};
    if (entityType === "Group") {
      options.id = core.getUuid();
    }
    return this._em.createEntity(entityType, options);
  }

  // get groups method that returns a promise for a group array
  // this method calling groups implements our application paging
  getGroups(page: number, pageSize: number): Promise<any> {
    // constructing a promise and returning it.
    let promise = new Promise<any>((resolve, reject) => {
      // declare a query variable and initialize it aganst the collection you are querying
      // executing your query against the entity manager which returns promises as breeze makes the calls for you.
      // breeze bridge angular facialtes convertion of default angular observables to promises, as breeze only uses promises.
      let query = EntityQuery.from("Groups")
        .expand("Site")
        .orderBy(["Name", "Active"])
        .skip((page - 1) * pageSize)
        .take(pageSize)
        .inlineCount();
      // our promise returns an array of group objects while our query an array of entity objects so we cast it to <any>
      this._em.executeQuery(query).then(
        queryResult => {
          this._groupsCached = true;
          resolve({
            groups: queryResult.results,
            totalRecords: queryResult.inlineCount
          });
        },
        error => reject(error)
      );
    });
    return promise;
  }

  // a method that gets a single group by Id allowing as to manipulate the single entity
  getGroup(GroupId: number): Promise<Group> {
    let promise = new Promise<Group>((resolve, reject) => {
      let query = EntityQuery.from("Groups").where(
        "GroupId",
        "equals",
        GroupId
      );
      let strategy: FetchStrategySymbol;
      if (!this._groupsCached) {
        strategy = FetchStrategy.FromServer;
      } else {
        strategy = FetchStrategy.FromLocalCache;
      }
      this._em.executeQuery(query.using(strategy)).then(
        response => {
          if (response.results && response.results.length == 1)
            resolve(<any>response.results[0]);
          else resolve(null);
        },
        error => reject(error)
      );
    });
    return promise;
  }

  getGroupById(GroupId: number) {
    return this._em.getEntityByKey("Group", GroupId);
  }

  // get contries method that returns a promise for a country array
  getCountries(): Promise<Country[]> {
    // constructing a promise and returning it.
    let promise = new Promise<Country[]>((resolve, reject) => {
      // declare a query variable and initialize it aganst the collection you are querying
      // executing your query against the entity manager which returns promises as breeze makes the calls for you.
      // breeze bridge angular facialtes convertion of default angular observables to promises, as breeze only uses promises.
      let query = EntityQuery.from("Countries")
      .expand('Regions.Districts');
      // our promise returns an array of country objects while our query an array of entity objects so we cast it to <any>
      this._em
        .executeQuery(query)
        .then(
          queryResult => resolve(<any>queryResult.results),
          error => reject(error)
        );
    });
    return promise;
  }

  // get country method with a country id passed in
  // 1st retrieval goes into the cache to look up, if the data is found on cache no need to go back to server
  getCountry(CountryId: any): Promise<Country> {
    let promise = new Promise<Country>((resolve, reject) => {
      let query = EntityQuery.from("Countries")
        .expand('Regions.Districts')
        .where("CountryId", "equals", CountryId);

      let strategy: FetchStrategySymbol;
      if (!this._em.metadataStore.isEmpty()) {
        strategy = FetchStrategy.FromServer;
      } else {
        strategy = FetchStrategy.FromLocalCache;
      }
      this._em.executeQuery(query.using(strategy)).then(
        response => {
          if (response.results && response.results.length == 1)
            resolve(<any>response.results[0]);
          else resolve(<any>null);
        },
        error => reject(error)
      );
    });
    return promise;
  }

  // get districts method that returns a promise for a country array
  getDistricts(): Promise<District[]> {
    // constructing a promise and returning it.
    let promise = new Promise<District[]>((resolve, reject) => {
      // declare a query variable and initialize it aganst the collection you are querying
      // executing your query against the entity manager which returns promises as breeze makes the calls for you.
      // breeze bridge angular facialtes convertion of default angular observables to promises, as breeze only uses promises.
      let query = EntityQuery.from("Districts");
      // our promise returns an array of country objects while our query an array of entity objects so we cast it to <any>
      this._em
        .executeQuery(query)
        .then(
          queryResult => resolve(<any>queryResult.results),
          error => reject(error)
        );
    });
    return promise;
  }

  getSites(): Promise<Site[]> {
    // constructing a promise and returning it.
    let promise = new Promise<Site[]>((resolve, reject) => {
      let query = EntityQuery.from("Sites")
      .take(20);
      this._em
        .executeQuery(query)
        .then(
          queryResult => resolve(<any>queryResult.results),
          error => reject(error)
        );
    });
    return promise;
  }

  getSitesCountryId(CountryId): Promise<Site[]> {
    // constructing a promise and returning it.
    let promise = new Promise<Site[]>((resolve, reject) => {
      let query = EntityQuery.from("Sites")
      .take(20);
      this._em
        .executeQuery(query)
        .then(
          queryResult => resolve(<any>queryResult.results),
          error => reject(error)
        );
    });
    return promise;
  }

  // get seasons method that returns a promise for a season array
  getSeasons(): Promise<Season[]> {
    // constructing a promise and returning it.
    let promise = new Promise<Season[]>((resolve, reject) => {
      // declare a query variable and initialize it aganst the collection you are querying
      // executing your query against the entity manager which returns promises as breeze makes the calls for you.
      // breeze bridge angular facialtes convertion of default angular observables to promises, as breeze only uses promises.
      let query = EntityQuery.from("Seasons");
      // our promise returns an array of season objects while our query an array of entity objects so we cast it to <any>
      this._em
        .executeQuery(query)
        .then(
          queryResult => resolve(<any>queryResult.results),
          error => reject(error)
        );
    });
    return promise;
  }

  saveChanges() {
    let promise = new Promise((resolve, reject) => {
      this._em.saveChanges().then(() => resolve(), error => reject(error));
    });
    return promise;
  }
  // not fully implemented
  searchAsync(searchTerm: string, field: string): Promise<Group[]> {
    let promise = new Promise((resolve, reject) => {
      let pred: Predicate;
      if (field === "group") {
        pred = new Predicate("Name", FilterQueryOp.Contains, searchTerm);
      } else pred = new Predicate(field, FilterQueryOp.Contains, searchTerm);
      let query = EntityQuery.from("Groups").where(pred);
      this._em
        .executeQuery(query)
        .then(
          queryResult => resolve(<any>queryResult.results),
          error => reject(error)
        );
    });
    return <any>promise;
  }
}
