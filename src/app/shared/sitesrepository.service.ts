// imports for core angular components for the service
import { Injectable } from '@angular/core';

// imported breeze apis and methods
import {
          EntityManager, EntityQuery, Predicate, Validator,
          FilterQueryOp, FetchStrategy, FetchStrategySymbol,
          EntityState, Entity, core
        } from 'breeze-client';

/// use entity-model class "acting as a barrel module instead of importing several lines" ///
import { Country, District, Season, Site, Group } from '../model/entity-model';

// imports for data model definations
import { RegistrationHelper } from '../model/registration-helper';

@Injectable()
export class SitesRepositoryService {
  // create an instance of an entity manager as a member variable to the class,
  // it takes the url to our API endpoint.
  private _em: EntityManager = new EntityManager(
    "http://localhost:5660/breeze/Group"
  );

  // create a functionality to handle the cache of your data with a boolean property
  private _sitesCached: boolean;

 // register our application metadata
  constructor() {
    RegistrationHelper.register(this._em.metadataStore);
  }

  // get site method that returns a promise for a site array
  getSites(): Promise<Site[]> {
    // constructing a promise and returning it.
    let promise = new Promise<Site[]>((resolve, reject) => {
      let query = EntityQuery.from("Sites");
      this._em
        .executeQuery(query)
        .then(
          queryResult => resolve(<any>queryResult.results),
          error => reject(error)
        );
    });
    return promise;
  }
}
