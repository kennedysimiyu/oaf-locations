// our application init guard to guard our components before routing
import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild } from '@angular/router';

import { GroupsRepositoryService } from './groupsrepository.service';

@Injectable()
export class InitGuard implements CanActivate, CanActivateChild {

  // set groupRepository service as a private member using the constructor
  constructor(private _groupsRepository: GroupsRepositoryService) {}

  // create methods that you will use to check on before activating your component routes.
  canActivate(): Promise<boolean> {
    return this._groupsRepository.initialize();
  }

  canActivateChild(): Promise<boolean> {
    return this.canActivate();
  }
}
