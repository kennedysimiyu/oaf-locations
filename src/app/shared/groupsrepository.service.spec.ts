import {
  async, inject, TestBed
} from '@angular/core/testing';

import {
 MockBackend,
 MockConnection
} from '@angular/http/testing';

import {
 HttpModule, Http, XHRBackend, Response, ResponseOptions
} from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';

/// use entity-model class "acting as a barrel module instead of importing several lines" ///
import { Country, District, Season, Site, Group } from '../model/entity-model';

import { GroupsRepositoryService as GroupsService } from './groupsrepository.service';

const makeSiteData = () => [
  {
    'DistrictId': 1104,
    'Name': 'U Aung Myint Oo',
    'SectorId': 1,
    'FirstSeason': 200,
    'LastSeason': null,
    'Active': true,
    'SiteId': 1,
    'SiteProjectCode': 'Site 1',
    'Coordinates': null,
    'DateModified': null,
    'Sector': null,
    'Groups': null,
    'Repayments': null,
    'District': null,
    'Season': null,
    'SiteEmployees': null,
    'QualifiedSiteName': 'U Aung Myint Oo (Active)'
  },

   {
    'DistrictId': 1104,
    'Name': 'U Kyi Khine',
    'SectorId': 1,
    'FirstSeason': 200,
    'LastSeason': null,
    'Active': true,
    'SiteId': 2,
    'SiteProjectCode': 'Site 2',
    'Coordinates': null,
    'DateModified': null,
    'Sector': null,
    'Groups': null,
    'Repayments': null,
    'District': null,
    'Season': null,
    'SiteEmployees': null,
    'QualifiedSiteName': 'U Kyi Khine (Active)'
    }
  ] as Site[];




////////  Tests  /////////////
describe('GroupsRepositoryService (mockBackend)', () => {

 beforeEach( async(() => {
   TestBed.configureTestingModule({
     imports: [ HttpModule ],
     providers: [
      GroupsService,
       { provide: XHRBackend, useClass: MockBackend }
     ]
   })
   .compileComponents();
 }));

 it('can instantiate service when inject service',
   inject([GroupsService], (service: GroupsService) => {
     expect(service instanceof GroupsService).toBe(true);
 }));



 it('can instantiate service with "new"', inject([Http], (http: Http) => {
   expect(http).not.toBeNull('http should be provided');
   let service = new GroupsService(http);
   expect(service instanceof GroupsService).toBe(true, 'new service should be ok');
 }));


 it('can provide the mockBackend as XHRBackend',
   inject([XHRBackend], (backend: MockBackend) => {
     expect(backend).not.toBeNull('backend should be provided');
 }));

 describe('when getSites', () => {
     let backend: MockBackend;
     let service: GroupsService;
     let fakeSites: Site[];
     let response: Response;

     beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
       backend = be;
       service = new GroupsService(http);
       fakeSites = makeSiteData();
       let options = new ResponseOptions({status: 200, body: {data: fakeSites}});
       response = new Response(options);
     }));

     it('should have expected fake sites (then)', async(inject([], () => {
       backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

       service.getSites().toPromise()
       // .then(() => Promise.reject('deliberate'))
         .then(sites => {
           expect(sites.length).toBe(fakeSites.length,
             'should have expected no. of sites');
         });
     })));

     it('should have expected fake sites (Observable.do)', async(inject([], () => {
       backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

       service.getSites()
         .do(sites => {
           expect(sites.length).toBe(fakeSites.length,
             'should have expected no. of sites');
         })
         .toPromise();
     })));


     it('should be OK returning no sites', async(inject([], () => {
       let resp = new Response(new ResponseOptions({status: 200, body: {data: []}}));
       backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));

       service.getSites()
         .do(sites => {
           expect(sites.length).toBe(0, 'should have no sites');
         })
         .toPromise();
     })));

     it('should treat 404 as an Observable error', async(inject([], () => {
       let resp = new Response(new ResponseOptions({status: 404}));
       backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));

       service.getSites()
         .do(sites => {
           fail('should not respond with heroes');
         })
         .catch(err => {
           expect(err).toMatch(/Bad response status/, 'should catch bad response status code');
           return Observable.of(null); // failure is the expected test result
         })
         .toPromise();
     })));
 });
});
