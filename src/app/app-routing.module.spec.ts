/* tslint:disable:no-unused-variable */
import { SpyLocation } from '@angular/common/testing';
import {Location} from "@angular/common";
import {TestBed, fakeAsync, tick} from '@angular/core/testing';
import {RouterTestingModule} from "@angular/router/testing";
import {Router} from "@angular/router";

import { GroupsListComponent } from './groups/groups-list.component';
import { SectorsListComponent } from './sectors/sectors-list.component';
import { SitesListComponent } from './sites/sites-list.component';

import { AppRoutingModule } from './app-routing.module';

describe('Router: App', () => {
  let location: Location;
  let router: Router;
  let fixture;

  beforeEach(() => {
       TestBed.configureTestingModule({
            imports: [    RouterTestingModule.withRoutes(routes)],
             declarations: [
                GroupsListComponent,
                SectorsListComponent,
                SitesListComponent
                ]
              });

    router = TestBed.get(Router);
    location = TestBed.get(Location);

    fixture = TestBed.createComponent(AppRoutingModule);
    router.initialNavigation();
   });
/*  it('fakeAsync works', fakeAsync(() => {
       let promise = new Promise((resolve) => {
            setTimeout(resolve, 10)
         });

          let done = false;
          promise.then(() => done = true);
          tick(50);
          expect(done).toBeTruthy();
         })); */

 /* it('navigate to "" redirects you to /home', fakeAsync(() => {
       router.navigate(['']);
       tick(50);
       expect(location.path()).toBe('/home');
        })); */

 /* it('navigate to "search" takes you to /search', fakeAsync(() => {
       router.navigate(['/search']);
       tick(50);
       expect(location.path()).toBe('/search');
       })); */
});
