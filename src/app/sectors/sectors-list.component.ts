// main sectors component
import { Component, OnInit } from '@angular/core';

import { SectorsRepositoryService } from '../shared/sectorsrepository.service';
import { Sector } from '../model/sector';


@Component({
  selector: 'oaf-sectors-list',
  templateUrl: './sectors-list.component.html'
})
export class SectorsListComponent implements OnInit {
  // declaration of the groupsRepository Service instance that gets dependency injected, and declares a member variable in one statement
  constructor(private sectorsRepositoryService: SectorsRepositoryService) {}

  // class property, array of sectors
  sectors: Sector[];

   // method called by angular when you component is loaded it can be placed on the view or through client side routing.
   ngOnInit() {
    // call a method on the sectors repository service to get the sectors. this method handles the promise
     this.sectorsRepositoryService.getSectors().then(sectors => this.sectors,
                                                   error => console.error(error));
    }
  }


