// imported angular core components
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// imported third party modules e.g breeze, ng2 bootstrap
import { BreezeBridgeHttpClientModule } from 'breeze-bridge2-angular';
import { NamingConvention } from 'breeze-client';
import { ModalModule } from 'ngx-bootstrap';

// imported application components
import { AppComponent } from './app.component';
import { SectorsListComponent } from './sectors/sectors-list.component';
import { SitesListComponent } from './sites/sites-list.component';

    // imported application components for groups tool
import { GroupsListComponent } from './groups/groups-list.component';
import { GroupsListItemComponent } from './groups/groups-list-item/groups-list-item.component';
import { AddNewGroupComponent } from './groups/add-new-group/add-new-group.component';

  // imported application components for navigation slectors tool
import { NavigationComponent } from './navigation/navigation.component';
import { CountryNavigationSelectionComponent } from './navigation/country-navigation-selection/country-navigation-selection.component';
import { DistrictNavigationSelectionComponent } from './navigation/district-navigation-selection/district-navigation-selection.component';
import { SeasonNavigationSelectionComponent } from './navigation/season-navigation-selection/season-navigation-selection.component';



// imported application services
import { GroupsRepositoryService } from './shared/groupsrepository.service';
import { SectorsRepositoryService } from './shared/sectorsrepository.service';
import { SitesRepositoryService } from './shared/sitesrepository.service';

import { InitGuard } from './shared/init.guard';

// imported application routing module
import { AppRoutingModule } from './app-routing.module';
import { GroupconcatsitesPipe } from './shared/groupconcatsites.pipe';
import { AddNewSiteComponent } from './sites/add-new-site/add-new-site.component';
import { LocationsSearchComponent } from './locations-search/locations-search.component';

@NgModule({

  declarations: [
              // main application component declaration
              AppComponent,

              // application sector component declarations
              SectorsListComponent,
              SitesListComponent,

              // application group component declarations
              GroupsListComponent,
              GroupsListItemComponent,
              AddNewGroupComponent,

              // application navigation component declarations
              NavigationComponent,
              CountryNavigationSelectionComponent,
              DistrictNavigationSelectionComponent,
              SeasonNavigationSelectionComponent,

              // application pipes
              GroupconcatsitesPipe,

              AddNewSiteComponent,

              LocationsSearchComponent

              ],
  imports:  [
              BrowserModule,
              FormsModule,
              BreezeBridgeHttpClientModule,
              ModalModule.forRoot(),
              HttpClientModule,
              AppRoutingModule
             ],
  providers: [
              //
              GroupsRepositoryService,
              SectorsRepositoryService,
              SitesRepositoryService,
              InitGuard
             ],
  bootstrap: [
              AppComponent
             ]
})

export class AppModule {
  constructor() {
  /*
    the naming convention in .Net which our api uses is Pascal Case by default
      1. Use internally newtonsoft.json to convert to camelCase which javascript uses
      2. Use our breeze naming convention
  */

    // NamingConvention.camelCase.setAsDefault();
  }
 }
