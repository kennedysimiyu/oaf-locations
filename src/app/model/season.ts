// model class for season properties from the database
export class Season {

  DistrictId: number;
  SeasonId: number;
  SeasonName: string;
  FirstTrimester: any;
  Active: boolean;
  SecondTrimester: any;
  ThirdTrimester: any;
  USDSeasonalExchangeRate: number;
  OAFOperationalYear: number;
  UniqueSeasonId: any;
  SeasonClients: any;
  SeasonFees: any;
  VSeasonClientCredits: any;
  ClientDeliveryDrops: any;
  District: any;
  CreditCycles: any;
}
