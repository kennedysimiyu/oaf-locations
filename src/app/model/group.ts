// model class for group properties from the database
export class Group {


  DistrictId: number;
  GroupId: number;
  Name: string;
  SiteId: number;
  Active: boolean;
  FirstSeason: number;
  LastSeason: number;
  District: string;
  Site: any;
  GroupDeliveryDrop: string;
  SeasonClients: string;
  Repayments: string;
}
