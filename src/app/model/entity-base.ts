// breeze APIs are registered here, our base class inherits from here and adds two entities
// entityAspect and entityType
import { Entity, EntityAspect, EntityType } from 'breeze-client';

export class EntityBase implements Entity {
// entity aspect helps breeze behind the scenes delete, invoke validation and track the state of the entity.
  entityAspect: EntityAspect;
  entityType: EntityType;

  get $typeName(): string {
    if (!this.entityAspect) return '';
    return this.entityAspect.getKey().entityType.shortName;
  }
}
