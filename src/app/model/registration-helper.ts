import { MetadataStore } from 'breeze-client';

// our models we want to register
import { Country } from './country';
import { District } from './district';
import { Group } from './group';
import { Season } from './season';
import { Sector } from './sector';
import { Site } from './site';

export class RegistrationHelper {
// set the breeze entity manager to use our types instead of what it generates by default
  static register(metadataStore: MetadataStore) {
    metadataStore.registerEntityTypeCtor('Country', Country);
    metadataStore.registerEntityTypeCtor('District', District);
    metadataStore.registerEntityTypeCtor('Group', Group);
    metadataStore.registerEntityTypeCtor('Season', Season);
    metadataStore.registerEntityTypeCtor('Sector', Sector);
    metadataStore.registerEntityTypeCtor('Site', Site);
  }
}
