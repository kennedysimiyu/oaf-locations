// model class for sector properties from the database
export class Sector {

 DistrictId: number;
 SectorId: number;
 Name: string;
 Active: boolean;
 FirstSeason: number;
 LastSeason: number;
 District: any;
 Season: any;
 Sites: any;
}
