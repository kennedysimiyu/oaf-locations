// model class for country properties from the database
export class Country {

  CountryId: number;
  Name: string;
  CurrencyCode: string;
  CountryTypeId: any;
  Regions: any;
}
