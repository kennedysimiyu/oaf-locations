// model class for district properties from the database
export class District {

  DistrictId: number;
  RegionId: number;
  Name: string;
  DistrictCode: string;
  Region: any;
  Sectors: any;
  SeasonClients: any;
  Deliveries: any;
  CreditCycles: any;
  ClientDeliveryDrops: any;
  DeliveryDropInputAuditLogs: any;
  VClientDeliveryDropInputs: any;
  Bundles: any;
  Repayments: any;
  Seasons: any;
  Groups: any;
  Sites: any;
  Clients: any;
  QualifiedDistrictName: string;
}
