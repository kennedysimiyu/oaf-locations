// model class for site properties from the database
export class Site {

  DistrictId: number;
  Name: string;
  SectorId: number;
  FirstSeason: number;
  LastSeason: number;
  Active: boolean;
  SiteId: number;
  SiteProjectCode: any;
  Coordinates: any;
  DateModified: any;
  Sector: any;
  Groups: any;
  Repayments: any;
  District: any;
  Season: any;
  SiteEmployees: any;
  QualifiedSiteName: any;
}
