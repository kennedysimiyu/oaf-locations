import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';

import { async, fakeAsync, ComponentFixture, TestBed, tick } from '@angular/core/testing';

import { By } from '@angular/platform-browser';

import { GroupsRepositoryService } from '../../shared/groupsrepository.service';
import { AddNewGroupComponent } from '../add-new-group/add-new-group.component';

describe('AddNewGroupComponent', () => {
  let component: AddNewGroupComponent;
  let fixture: ComponentFixture<AddNewGroupComponent>;

  let spy: jasmine.Spy;
  let de: DebugElement;
  let el: HTMLElement;
  let groupsRepositoryService: GroupsRepositoryService; // the actually injected service

  // async beforeEach for a component with an outside template
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewGroupComponent ], // declare the test component
      schemas:      [ NO_ERRORS_SCHEMA ],
      providers:    [ {provide: GroupsRepositoryService, useValue: groupsRepositoryService } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
