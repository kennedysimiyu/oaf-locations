import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Country, District, Season, Site, Group } from '../../model/entity-model';
import { RegistrationHelper } from '../../model/registration-helper';


import { GroupsRepositoryService } from '../../shared/groupsrepository.service';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.directive';

@Component({
  selector: 'oaf-add-new-group',
  templateUrl: './add-new-group.component.html',
  styleUrls: ['./add-new-group.component.css']
})
export class AddNewGroupComponent implements OnInit {
  private errorMessage: string;

  constructor(private _groupsRepository: GroupsRepositoryService) { }

  @ViewChild('deleteModal')
  public deleteModal: ModalDirective;

  @Input()
  groups: Group[];

  ngOnInit() {
    }

  deleteGroup() {
      this.deleteModal.show();
    }
  confirmDeleteGroup() {
    this.deleteModal.hide();
  //  this.group.entityAspect.setDeleted();
    this._groupsRepository.saveChanges();
  }


  onSave() {
   this._groupsRepository.saveChanges();
  }
}

