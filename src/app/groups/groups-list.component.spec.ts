import { CUSTOM_ELEMENTS_SCHEMA, DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';

import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { Router, ActivatedRoute } from '@angular/router';


import { GroupsRepositoryService } from '../shared/groupsrepository.service';

import { GroupsListComponent } from './groups-list.component';
import { NavigationComponent } from '../navigation/navigation.component';
import { ActivatedRouteSnapshot } from '@angular/router/src/router_state';

import { Observable } from 'rxjs/Rx';

describe('GroupsListComponent', () => {
  let component: GroupsListComponent;
  let fixture: ComponentFixture<GroupsListComponent>;
  let componentGroupsRepositoryService: GroupsRepositoryService; // the actually injected service

  let spy: jasmine.Spy;
  let de: DebugElement;
  let el: HTMLElement;
  let groupsRepositoryService: GroupsRepositoryService; // the actually injected service

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
                      GroupsListComponent,
                      NavigationComponent
                    ],
      schemas:      [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
      providers:    [ {provide: GroupsRepositoryService, useValue: groupsRepositoryService } ]
    })
    .compileComponents();
  }));


});

describe('Component Stubs', () => {
  let component: GroupsListComponent;
  let fixture: ComponentFixture<GroupsListComponent>;
  let groupsRepositoryServiceStub: any;

  let componentGroupsRepositoryService: GroupsRepositoryService; // the actually injected service

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
                      GroupsListComponent,
                      NavigationComponent
                    ],
      schemas:      [ NO_ERRORS_SCHEMA ],
      providers:    [ {provide: GroupsRepositoryService, useValue: groupsRepositoryServiceStub } ]
    })
    .compileComponents();
    groupsRepositoryServiceStub = {
      getCountries: jasmine.createSpy('getCountries').add.callFake(
        () => Promise.resolve(true).then(() => {
        })
      )
    };

    groupsRepositoryServiceStub = {
      getGroups: jasmine.createSpy('getGroups').add.callFake(
        () => Promise.resolve(true).then(() => {
        })
      )
    };

  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(GroupsListComponent);
    component = fixture.componentInstance;

    // UserService actually injected into the component
    groupsRepositoryServiceStub = fixture.debugElement.injector.get(GroupsRepositoryService);
    componentGroupsRepositoryService = groupsRepositoryServiceStub;
    // UserService from the root injector
    groupsRepositoryServiceStub = TestBed.get(GroupsRepositoryService);
    fixture.detectChanges();
  });
 /* describe('component onInit', () => {
    it('çan instatiate it', () =>
      expect(component).not.toBeNull();
    );
  }); */

 /* it('should create', () => {
    expect(component).toBeTruthy();
  }); */

});

