import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { By } from '@angular/platform-browser';

import { GroupsRepositoryService } from '../../shared/groupsrepository.service';
import { GroupsListItemComponent } from './groups-list-item.component';

describe('GroupsListItemComponent', () => {
  let component: GroupsListItemComponent;
  let fixture: ComponentFixture<GroupsListItemComponent>;

  let spy: jasmine.Spy;
  let de: DebugElement;
  let el: HTMLElement;
  let groupsRepositoryService: GroupsRepositoryService; // the actually injected service

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupsListItemComponent ],
      schemas:      [ NO_ERRORS_SCHEMA ],
      providers:    [ {provide: GroupsRepositoryService, useValue: groupsRepositoryService } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupsListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
