import { Component, OnInit, Input } from '@angular/core';

// use entity-model class "acting as a barrel module instead of importing several lines" ///
import { Country, District, Season, Site, Group } from '../../model/entity-model';

@Component({
  selector: 'oaf-groups-list-item',
  templateUrl: './groups-list-item.component.html',
  styleUrls: ['./groups-list-item.component.css']
})
export class GroupsListItemComponent implements OnInit {

  constructor() { }

// add a public class prperty to allow intput of data in the component from html template
  @Input()
  public group: Group;
  public country: Country;
  public district: District;
  public season: Season;
  public site: Site;

  ngOnInit() {
  }

}
