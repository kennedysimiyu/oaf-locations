// main groups component
import { Component, OnInit, ElementRef, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';


import { Router, ActivatedRoute } from '@angular/router';

import { GroupsRepositoryService } from '../shared/groupsrepository.service';

// use entity-model class "acting as a barrel module instead of importing several lines" ///
import { Country, District, Season, Site, Group } from '../model/entity-model';
import { RegistrationHelper } from '../model/registration-helper';
import { error } from 'util';

@Component({
  selector: 'oaf-groups-list',
  templateUrl: './groups-list.component.html',
  styleUrls: ['./groups-list.component.css']
})
export class GroupsListComponent implements OnInit {
  //
  public selectedCountryId = 0;
  private group: Group = new Group();

  private searchField: string = 'group';
  private searchInput: string;
  private currentPage: number = 1;
  private pageCount: number;
  private _totalRecords: number;
  private _pageSize: number = 20;
  // private group: Group = new Group();

  //
  private errorMessage: string;

  // declaration of the groupsRepository Service instance that gets dependency injected, and declares a member variable in one statement
  constructor(private _router: Router,
                private _route: ActivatedRoute,
                private _groupsRepositoryService: GroupsRepositoryService,
                private elementRef: ElementRef) {
                const eventStream = Observable.fromEvent(elementRef.nativeElement, 'keyup')
                .map(() => this.searchInput)
                .debounceTime(500)
                .distinctUntilChanged();
                eventStream.subscribe(input => this.search(input));
                 }

  // class property, array of our data from service repository
  groups: Group[];

  countries: Country[];
  districts: District[];
  seasons: Season[];
  sites: Site[];

  // method called by angular when you component is loaded it can be placed on the view or through client side routing.
  ngOnInit() {
 /* this.groupsRepositoryService.getCountry(CountryId).then(country => {
      this.country = country;
  }, error => {
     this.errorMessage = error.message;
  });    */
  // call a method on the groups repository service to get the customers. this method handles the promise
   this._groupsRepositoryService.getGroups(1, this._pageSize).then(result => {
      this.groups = result.groups;
      this._totalRecords = result.totalRecords;
      this.pageCount = Math.floor(this._totalRecords / this._pageSize);
      if (this.pageCount < (this._totalRecords / this._pageSize)) this.pageCount +=1;
   },
  error => console.error(error));

  // call a method on the countries repository service to get the countries. this method handles the promise
  this._groupsRepositoryService.getCountries().then(countries => {
    this.countries = countries;
  },
  error => console.error(error));

  this._groupsRepositoryService.getSeasons().then(seasons => {
    this.seasons = seasons;
  }, error => console.error(error));

  this._groupsRepositoryService.getSites().then(sites => {
    this.sites = sites;
  }, error => console.error(error));

  // call a method on the districts repository service to get the districts. this method handles the promise
  this._groupsRepositoryService.getDistricts().then(districts => {
    this.districts = districts;
  },
  error => console.error(error));

 // this.group = <Group>this._groupsRepositoryService.createEntity('Group');
  }

  pageUp() {
    if (this.currentPage * this._pageSize >= this._totalRecords) return;
    let newPage = this.currentPage + 1;
    this._groupsRepositoryService.getGroups(newPage, this._pageSize).then(result => {
      this.groups = result.groups;
      this.currentPage = newPage;
    }, error => console.error(error));

  }

  pageDown() {
    if (this.currentPage == 1) return;
    let  newPage = this.currentPage - 1;
    this._groupsRepositoryService.getGroups(newPage, this._pageSize).then(result => {
      this.groups = result.groups;
      this.currentPage = newPage;
    }, error => console.error(error));
  }


 search(value) {
    this._groupsRepositoryService.searchAsync(value, this.searchField).then(groups => {
         this.groups = groups;
    });
  }

  onSave() {
    this._groupsRepositoryService.saveChanges();
  }
}
